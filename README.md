Toy War

Toy War is an RTS game produced as a university assignment within a short period of time to 
demonstrate lighting, gameplay, input and sound capabilities. Users can control their players 
with mouse input and can move the camera around by moving the cursor near the edge of the screen 
in the direction you wish to move the camera or via kayboard input.

Commands:
	Keyboard:
		Up arrow - move the camera up
		Down arrow - move the camera down
		Left arrow - pan to the left
		Right arrow - pan to the right
	Mouse:
		Left click - select ogject
		Right click - perform action/move object/attack
	
	
Creedits:

In this project the ai and main 
game engine were produced mainly By Nuss, along with 
Ben Taylor, while I (Druce Taylor) implemented the 
audio, shaders, original input (keyboard and joystick, 
since replaced with mouse by Nuss) and other misc 
graphics. Jan Andruszkiewicz was kind enough to make 
the models for us.

Matthew Huffaker (http://www.youtube.com/user/teknoaxe), 
for permission to use his original suspense music 
content commercially. 

PLEASE NOTE: This game currently fails to run on Windows 10. It was originally made for Window 7.